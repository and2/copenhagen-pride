`import Ember from 'ember'`
`import RouteMetaMixin from 'ember-cli-meta-tags/mixins/route-meta'`

ArticleRoute = Ember.Route.extend RouteMetaMixin,

	model: (data) ->
		@controllerFor('application').set 'language', data.locale if data.locale isnt @controllerFor('application').language and data.locale in ['en', 'da']
		m = @controllerFor('application').articles.findBy 'slug', data.slug
		@controllerFor('application').set 'currentModel', m
		return m

	setupController: (controller, model)->
		@controllerFor('application').set 'transparentMenu', true
		@controllerFor('application').send 'hideMenu'

		model.category = 5
		controller.set 'model', model

	meta: ->
		data = @modelFor(@routeName)
		return {	
			property:
				'og:name': data.title_translated
				'og:url': document.location.href
				'og:image': data.pictures[2].url

			name:
				'twitter:image': data.pictures[2].url	
		}

	titleToken: (model) ->
		model.title_translated	

`export default ArticleRoute`
