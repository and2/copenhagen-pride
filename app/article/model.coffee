`import DS from 'ember-data'`

Article = DS.Model.extend {
  title: DS.attr('string')
  title_dk: DS.attr('string')
  content: DS.attr('string')
  content_dk: DS.attr('string')
  slug: DS.attr('string')
}

`export default Article`
