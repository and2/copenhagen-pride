`import Ember from 'ember'`

ExpandedMenuComponent = Ember.Component.extend
	tagName: 'nav'
	classNameBindings: ['visible']

	resetMenuItems: ->
		#reset previous styles
		@get('menuLines').setEach 'hideSubpages', true
		$('.text, .menu-wrapper>li').attr 'style', ''


	actions: 
		menuClick: (cls) ->

			if cls is 'l6' 
				return window.open 'https://dk.betternow.org/en/projects/copenhagenpride-projekt'

			return true if !@get('menuLines').findBy('cls', cls).hasSubpages

			@resetMenuItems()

			#loog for current li element
			Ember.set @get('menuLines').findBy('cls', cls), 'hideSubpages', false
			th = $('.'+cls+' .text').height() + $('.'+cls+' .text .subpages').height()

			height = $(window).height()
			if window.innerHeight
				height = if $(window).height() > window.innerHeight then $(window).height() else window.innerHeight

			hdiff = th+th*0.2 - height/6

			$('.menu-wrapper>.'+cls).css
				'height': th+th*0.2	


			$('.'+cls+' .text').css 
				'margin-top': -th/2


			$('.menu-wrapper>li').each (index, el) =>
				if !$(el).hasClass(cls)
					$(el).height height/6-hdiff/5


		closeMenu: ->		
			@set 'visible', false
			Ember.run.later =>
				@resetMenuItems()
			, 0	


		

`export default ExpandedMenuComponent`
