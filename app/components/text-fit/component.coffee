`import Ember from 'ember'`

TextFitComponent = Ember.Component.extend
	#classNameBindings: ['boxClass']

	didInsertElement: ->

		@get('resizeService').on 'debouncedDidResize', (ev) =>
			Ember.run.later => #wait for the parent to resize
				@setSize()
			, 0 	

		Ember.run.schedule 'afterRender', =>
			@setSize()

	willDestroyElement: ->
		@get('resizeService').off 'debouncedDidResize'		


	hasTextChanged:(->
		Ember.run.schedule 'afterRender', =>
			@setSize()
		return
	).observes('text')

	setSize: ->

		fs = @get('maxFontSize')
		el = $(@get('element')).find('.text-fit')
		parent = el.parents( @get('parent') )

		#console.log el.outerHeight(), parent.height()

		el.attr('style', '').css 'font-size', fs

		while (el.outerHeight() > parent.height()) or (el.find('span').width() > el.width())
			fs--
			el.css 'font-size', fs

		if @get('middle')

			el.css 
				top: '50%'
				'marginTop': -el.outerHeight()/2

`export default TextFitComponent`
