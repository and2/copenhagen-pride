`import Ember from 'ember'`

SmartImageComponent = Ember.Component.extend
	
	classNames: ['smart-image']

	init: ->
		pixel_ratio = if window.devicePixelRatio then window.devicePixelRatio else 1
		pixel_ratio = 1 if pixel_ratio < 1
		@pixel_ratio = pixel_ratio

		if @get('wide_box')
			@wideBoxSpecific()

		if @get('fullscreen')
			@fullscreenSpecific()
			

		@_super()


	didInsertElement: ->

		@get('resizeService').on 'debouncedDidResize', (ev) =>
			@setBackground() #if @get('wide_box')

		Ember.run.schedule 'afterRender', =>
			@setBackground() #if @get('wide_box')


	willDestroyElement: ->
		@get('resizeService').off 'debouncedDidResize'



	fullscreenSpecific: ->
		@ar = $(window).height()/$(window).width()
		@set 'paddingTop', (@ar*100)+'%'


	wideBoxSpecific: ->
		if (typeof @get('aspect_ratio')) is 'number'
			@ar = @get('aspect_ratio')
		
		else

			ars = @get('aspect_ratio')

			for item in ars 
				@ar = item.ar if $(window).width() < item.width

		if @get('full_box')
			parent = $(@get('element')).parents(@get('parent'))
			parent_height = parent.outerHeight()
			parent_width = parent.outerWidth()
			current_height = $(@get('element')).width()*@ar	


			if parent_height > current_height
				@ar = parent_height/parent_width
			else	
				parent.css 'min-height', current_height

		@set 'paddingTop', (@ar*100)+'%'

		

		

	setBackground: ->
		@fullscreenSpecific() if @get('fullscreen')

		@wideBoxSpecific() if @get('wide_box')


		width = $(@get('element')).width() * @pixel_ratio
		height = width * @ar * @pixel_ratio

		pictures = @get('pictures')

		bg = pictures[pictures.length-1].url

		for picture, i in pictures 
			if width <= picture.width and height <= picture.height 
				bg = picture.url
				break

		@set 'backgroundImage', bg		

`export default SmartImageComponent`
