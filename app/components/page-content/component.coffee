`import Ember from 'ember'`

PageContentComponent = Ember.Component.extend
	classNameBindings: ['category1', 'category2', 'category3', 'category4', 'category5', 'category6']
	classNames: ['page-content']	

	init: ->

		#@changeClass()		

		@get('resizeService').on 'didResize', (ev) =>
			@onNewElement()

		@_super()


		Ember.run.schedule 'afterRender', =>
			@onNewElement()
			@changeClass()

	modelChanged: (->
		@changeClass()
		@onNewElement()
	).observes('page.id')


	didInsertElement: ->
		Ember.run.schedule 'afterRender', =>
			#@onNewElement()

	willDestroyElement: ->
		@get('resizeService').off 'didResize'	
	
	changeClass: ->
		return if !@get('page').category
		#set the category class to the components root div to change the color
		@set 'category'+i, false for i in [1..6]
		@set 'category'+@get('page').category, true

	onNewElement: ->
		$('.page-content').css 'min-height', $(window).height()	
		$('iframe').height $('.page-content').width()*0.5625



`export default PageContentComponent`
