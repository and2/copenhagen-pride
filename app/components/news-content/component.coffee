`import Ember from 'ember'`

NewsContentComponent = Ember.Component.extend
	classNames: ['news-content']

	init: ->
		@get('resizeService').on 'didResize', (ev) =>
			@onResize()

		Ember.run.schedule 'afterRender', =>
			@onResize()	

		@_super()

	onResize: ->
		$('.news-content').css 'min-height', $(window).height()	

	willDestroyElement: ->
		@get('resizeService').off 'didResize'	

	actions:
		showArticle: (slug) ->
			@sendAction 'action', slug

`export default NewsContentComponent`
