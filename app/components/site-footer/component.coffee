`import Ember from 'ember'`

SiteFooterComponent = Ember.Component.extend
	tagName: 'footer'
	classNameBindings: ['transparentMenu']

	actions:
		changeLanguage: (lang) ->
			@set 'language', lang

		newtab: (address) ->
			window.open address	

`export default SiteFooterComponent`
