`import Ember from 'ember'`

NewsRowComponent = Ember.Component.extend

	ars: [{ar: 0.44, width: 10000}, {ar: 0.6, width: 600}]

	actions:

		showArticle: (slug) ->
			@sendAction 'action', slug

`export default NewsRowComponent`
