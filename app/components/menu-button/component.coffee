`import Ember from 'ember'`

MenuButtonComponent = Ember.Component.extend

	mouseEnter: ->
			$('.menu-line').addClass 'hover'

	mouseLeave: ->
		$('.menu-line').removeClass 'hover'

	click: ->
		@sendAction()

`export default MenuButtonComponent`
