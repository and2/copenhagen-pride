`import Ember from 'ember'`

SmartVideoComponent = Ember.Component.extend
	classNames: ['smart-video']

	init: ->
		@fullscreenSpecific()
		@_super()

	didInsertElement: ->
		Ember.run.schedule 'afterRender', =>
			@setVideo()

		@get('resizeService').on 'debouncedDidResize', (ev) =>
			@fullscreenSpecific() 

	willDestroyElement: ->
		@get('resizeService').off 'debouncedDidResize'			

	fullscreenSpecific: ->
		@ar = $(window).height()/$(window).width()
		@set 'paddingTop', (@ar*100)+'%'

	setVideo: ->		

		$(@get('element')).find('.fullscreen-box').vide {
			poster: @get('pictures')[@get('pictures').length-1].url
			mp4: @get('video').mp4
			webm: @get('video').webm
		},
		{
			muted: true
			loop: true
			autoplay: true 
			position: "50% 50%"
		}

`export default SmartVideoComponent`
