`import Ember from 'ember'`

OurSponsorsComponent = Ember.Component.extend

	mouseEnter: ->
		Ember.set @get('sponsor'), 'hover', true

	mouseLeave: ->
		Ember.set @get('sponsor'), 'hover', false

	click: ->
		window.open @get('sponsor').site		

`export default OurSponsorsComponent`
