`import Ember from 'ember'`

HomeController = Ember.Controller.extend

	application: Ember.inject.controller()

	arr1: []
	arr2: []

	shuffle: (array) ->
		currentIndex = array.length
		temporaryValue = undefined
		randomIndex = undefined
		# While there remain elements to shuffle...
		while 0 != currentIndex
			# Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex)
			currentIndex -= 1
			# And swap it with the current element.
			temporaryValue = array[currentIndex]
			array[currentIndex] = array[randomIndex]
			array[randomIndex] = temporaryValue
		array


	init: ->
		#get featured pages and the same number of news
		pages = PrideData.pages.filterBy 'feature', true
		pages = @shuffle(pages)
		pages = pages.slice 0, if pages.length%2 is 0 then pages.length-1 else pages.length

		if pages.length > 0
			pages.push { excerpt_translated: @get('application').menuLines[5].text, donate: true, category: 6, feature: true}

		news  = PrideData.articles.slice 0, 6
		news = news.slice 0, if news.length%2 is 0 then news.length else news.length-1

		tweets = []
		PrideData.tweets.forEach (tweet) =>
			tweets.push {text: tweet.text, tweet: true}

		tweets = tweets.slice 0, 4

		###console.log pages
		console.log news###

		if pages.length > 0
			for i in [1..pages.length/2]
				@arr1.push pages[i-1]
			for j in [pages.length/2+1..pages.length]
				@arr2.push pages[j-1]	

		if news.length > 0
			for i in [1..news.length/2]
				@arr1.push news[i-1]

			for j in [news.length/2+1..news.length]	
				@arr2.push news[j-1]	

		if tweets.length > 0
			for i in [1..tweets.length/2]
				@arr1.push tweets[i-1]	
			for j in [tweets.length/2+1..tweets.length]	
				@arr2.push tweets[j-1]	
			#@arr2.push tweets[j-1]	

		@columns = [ {entries: @shuffle(@arr1)}, {entries: @shuffle(@arr2)} ]


	actions: 
		click: (entry) ->

			return window.open 'https://dk.betternow.org/en/projects/copenhagenpride-projekt' if entry.category is 6

			return window.open('https://twitter.com/CopenhagenPride') if entry.tweet

			@transitionToRoute( (if entry.feature then 'page' else 'article'), @locale, entry.slug)


	

`export default HomeController`
