`import Ember from 'ember'`
`import RouteMetaMixin from 'ember-cli-meta-tags/mixins/route-meta'`

HomeRoute = Ember.Route.extend RouteMetaMixin,
	
	model: (data)->
		@controllerFor('application').set 'currentModel', null
		@controllerFor('application').set 'language', data.locale if data.locale isnt @controllerFor('application').language and data.locale in ['en', 'da']
		@controllerFor('home').set 'locale', data.locale

	setupController: (model, controller) ->
		@controllerFor('application').set 'transparentMenu', false

	meta: ->
		data = @controllerFor('application').home
		return {	
			property:
				'og:name': 'Copenhagen Pride'
				'og:description': data.title_translated
				'og:url': document.location.href
				'og:image': data.pictures[2].url

			name:
				'description': data.title_translated
				'twitter:image': data.pictures[2].url	
		}

	title: 'Copenhagen Pride'			

`export default HomeRoute`
