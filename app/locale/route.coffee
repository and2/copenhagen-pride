`import Ember from 'ember'`

LocaleRoute = Ember.Route.extend
	model: (data)->

	setupController: (model, controller) ->
		locale = (window.navigator.userLanguage || window.navigator.language).slice(0,2)
		if locale in ['en', 'da'] then else locale='da'
		@controllerFor('application').set 'language', locale
		@controllerFor('application').transitionToRoute 'home', @controllerFor('application').language 


`export default LocaleRoute`
