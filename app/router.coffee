`import Ember from 'ember';`
`import config from './config/environment';`


Router = Ember.Router.extend
	location: 'hashbang'
	

Router.map ->
  @route 'home', {path: '/:locale/home'}

  @route 'page', {path: '/:locale/page/:slug'}

  @route 'news', {path: '/:locale/news'}	
 
  @route 'article', {path: '/:locale/article/:slug'}

  @route 'locale', {path: '/'}

`export default Router;`
