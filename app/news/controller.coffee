`import Ember from 'ember'`

NewsController = Ember.Controller.extend

	application: Ember.inject.controller()

	actions:

		showArticle: (slug) ->
			@transitionToRoute 'article', @locale, slug

`export default NewsController`
