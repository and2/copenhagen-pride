`import Ember from 'ember'`
`import RouteMetaMixin from 'ember-cli-meta-tags/mixins/route-meta'`

NewsRoute = Ember.Route.extend RouteMetaMixin,

	model: (data) ->
		@controllerFor('application').set 'currentModel', null
		@controllerFor('application').set 'language', data.locale if data.locale isnt @controllerFor('application').language and data.locale in ['en', 'da']

		@controllerFor('news').set 'locale', data.locale


	setupController: (controller, model)->
		@controllerFor('application').set 'transparentMenu', true
		@controllerFor('application').send 'hideMenu'

		#console.log @controllerFor('application').articles
		
	meta: ->
		data = @controllerFor('application').menuLines
		return {	
			property:
				'og:name': data[3].text
				'og:url': document.location.href

		}

	titleToken: ->
		data = @controllerFor('application').menuLines
		data[3].text


`export default NewsRoute`
