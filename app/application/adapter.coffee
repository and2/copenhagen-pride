`import DS from 'ember-data'`
`import config from '../config/environment';`

ApplicationAdapter = DS.JSONAPIAdapter.extend
	host: config.apiURL
	isNewSerializerAPI: true

	shouldBackgroundReloadRecord: ->
		return false

	shouldReloadAll: ->
		return false	

`export default ApplicationAdapter`
