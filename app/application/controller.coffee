`import Ember from 'ember'`

ApplicationController = Ember.Controller.extend

	needs: ['page']

	menuIsVisible: false

	menuLines: [
		{
			cls: 'l1' 
			text_en: 'The Parade'
			text_dk: 'Paraden' 
			pages: []
			hasSubpages: true
			hideSubpages: true	
			category: 1
		}

		{
			cls: 'l2'
			text_en: 'Volunteer'
			text_dk: 'Bliv frivillig'
			pages: [
			]
			hasSubpages: true
			hideSubpages: true
			category: 2
		}

		{
			cls: 'l3', 
			text_en: 'Sponsor'
			text_dk: 'Bliv sponsor'
			pages: []			
			hasSubpages: true
			hideSubpages: true
			category: 3
		}
		{cls: 'l4', text_en: 'News', text_dk: 'Nyheder', news:true}
		{
			cls: 'l5', 
			text_en: 'About'
			text_dk: 'Om os'
			pages: []			
			hasSubpages: true
			hideSubpages: true
			category: 4
		}
		{cls: 'l6', text_en: 'Donate', text_dk: 'Donér', donate: true}
	]

	language: 'en'

	transparentMenu: false

	init: ->
		FastClick.attach(document.body)

		@set 'language', 'en'	

		@articles = PrideData.articles

		@articles.forEach (article, index) =>

			article.line = true if index+1 < @articles.length and !@articles[index+1].pictures

		@home = PrideData.home

		@sponsors = PrideData.sponsors
		

	setTranslations: (->

		#setup the menu lines
		#and the pages cached for each section of the menu
		@menuLines.forEach (item) =>
			#translate menu titles
			Ember.set item, 'text', if @language is 'en' then item.text_en else item.text_dk

			#translate pages (title, excerpt, content)
			Ember.set item, 'pages', PrideData.pages.filterBy('category', item.category)
			item.pages.map (p) => 
				Ember.set p, 'title_translated', if @language is 'en' then p.title else p.title_dk
				Ember.set p, 'excerpt_translated', if @language is 'en' then p.excerpt else p.excerpt_dk
				Ember.set p, 'content_translated', if @language is 'en' then p.content else p.content_dk

				Ember.set p, 'sponsorsPage', if p.slug is 'our-sponsors' and p.category is 3 then true else false
				return p

		@articles.map (p) =>
			Ember.set p, 'title_translated', if (@language is 'en' and p.title isnt '') then p.title else p.title_dk
			Ember.set p, 'content_translated', if @language is 'en' and p.content isnt '' then p.content else p.content_dk
			Ember.set p, 'no_picture', true if !p.pictures
			return p

		@sponsors.map (p) =>
			Ember.set p, 'how_translated', if @language is 'en' then p.how_en else p.how_dk	

		Ember.set @home, 'title_translated', if @language is 'en' then @home.title else @home.title_dk	
		Ember.set @home, 'has_video', if @home.category is 2 then true else false


		#set the new url
		#this is ugly, needs a better solution
		if @get('currentRouteName')
			currentRouteModel = @get('currentModel')
			@transitionToRoute @get('currentRouteName'), @language if !currentRouteModel
			@transitionToRoute @get('currentRouteName'), @language, currentRouteModel.slug if currentRouteModel

		#console.log @controllerFor(@get('currentRouteName')) if @get('currentRouteName')
				
	).observes('language').on('init')	

	
	hasChagedUrl: ( ->
		$(document).scrollTop 0

	).observes('currentRouteName', 'currentModel')


	actions:
		showMenu: ->
			@set 'menuIsVisible', true
			return

		hideMenu: ->
			@set 'menuIsVisible', false	


`export default ApplicationController`
