`import Ember from 'ember'`

ApplicationRoute = Ember.Route.extend
	title: (tokens) ->
		tokens.join(' | ') + ' | Copenhagen Pride'

`export default ApplicationRoute`
