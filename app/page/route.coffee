`import Ember from 'ember'`
`import RouteMetaMixin from 'ember-cli-meta-tags/mixins/route-meta'`

PageRoute = Ember.Route.extend RouteMetaMixin,

	model: (data) ->
		@controllerFor('application').set 'language', data.locale if data.locale isnt @controllerFor('application').language and data.locale in ['en', 'da']
		m = PrideData.pages.findBy 'slug', data.slug
		@controllerFor('application').set 'currentModel', m
		@controllerFor('news').set 'locale', data.locale
		return m

	setupController: (controller, model)->
		@controllerFor('application').set 'transparentMenu', true
		@controllerFor('application').send 'hideMenu'

		model.sponsors = @controllerFor('application').sponsors if model.sponsorsPage
		controller.set 'model', model

	meta: ->
		data = @modelFor(@routeName)
		return {	
			property:
				'og:name': data.title_translated
				'og:description': data.excerpt_translated
				'og:url': document.location.href
				'og:image': data.pictures[2].url if data.pictures

			name:
				'description': data.excerpt_translated
				'twitter:image': data.pictures[2].url if data.pictures
		}

	titleToken: (model) ->
		model.title_translated


`export default PageRoute`
