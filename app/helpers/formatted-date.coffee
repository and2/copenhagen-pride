`import Ember from 'ember'`

# This function receives the params `params, hash`
formattedDate = (params) ->

	d = new Date(params)
	return d.getDate()+'/'+d.getMonth()+'/'+d.getFullYear()

FormattedDateHelper = Ember.Helper.helper formattedDate


`export { formattedDate }`

`export default FormattedDateHelper`
