`import Ember from 'ember'`
`import TestMixin from '../../../mixins/test'`
`import { module, test } from 'qunit'`

module 'Unit | Mixin | test'

# Replace this with your real tests.
test 'it works', (assert) ->
  TestObject = Ember.Object.extend TestMixin
  subject = TestObject.create()
  assert.ok subject
