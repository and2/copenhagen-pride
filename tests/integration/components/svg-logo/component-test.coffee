`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'svg-logo', 'Integration | Component | svg logo', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{svg-logo}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#svg-logo}}
      template block text
    {{/svg-logo}}
  """

  assert.equal @$().text().trim(), 'template block text'
