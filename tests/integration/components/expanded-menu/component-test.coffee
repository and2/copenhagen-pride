`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'expanded-menu', 'Integration | Component | expanded menu', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{expanded-menu}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#expanded-menu}}
      template block text
    {{/expanded-menu}}
  """

  assert.equal @$().text().trim(), 'template block text'
