`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'news-row', 'Integration | Component | news row', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{news-row}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#news-row}}
      template block text
    {{/news-row}}
  """

  assert.equal @$().text().trim(), 'template block text'
