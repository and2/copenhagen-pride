`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'rainbow-background', 'Integration | Component | rainbow background', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{rainbow-background}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#rainbow-background}}
      template block text
    {{/rainbow-background}}
  """

  assert.equal @$().text().trim(), 'template block text'
