`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'smart-video', 'Integration | Component | smart video', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{smart-video}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#smart-video}}
      template block text
    {{/smart-video}}
  """

  assert.equal @$().text().trim(), 'template block text'
