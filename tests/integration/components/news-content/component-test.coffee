`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'news-content', 'Integration | Component | news content', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{news-content}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#news-content}}
      template block text
    {{/news-content}}
  """

  assert.equal @$().text().trim(), 'template block text'
