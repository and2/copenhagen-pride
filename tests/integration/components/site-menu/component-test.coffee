`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'site-menu', 'Integration | Component | site menu', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{site-menu}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#site-menu}}
      template block text
    {{/site-menu}}
  """

  assert.equal @$().text().trim(), 'template block text'
