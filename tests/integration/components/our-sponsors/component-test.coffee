`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'our-sponsors', 'Integration | Component | our sponsors', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{our-sponsors}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#our-sponsors}}
      template block text
    {{/our-sponsors}}
  """

  assert.equal @$().text().trim(), 'template block text'
