module.exports = (grunt) ->
	require('time-grunt')(grunt)
	require('jit-grunt')(grunt,{
		'exec': 'grunt-exec'
	})

	grunt.initConfig
		pkg: grunt.file.readJSON 'package.json'


		exec:
			copy_index:
				command: 'cp dist/index.html ~/Work/2015/pride-api/app/views/html/index.html'
			commit:
				command: 'cd ~/Work/2015/pride-api && git commit -am "updated index"'
			deploy:
				command: 'cd ~/Work/2015/pride-api && eb deploy'


		compress:
			main:
				options:
					mode: 'gzip'
				files: [
					{expand: true, cwd: 'dist/assets/', src: ['*.js'], dest: 'dist/assets/compressed'}
					{expand: true, cwd: 'dist/assets/', src: ['*.css'], dest: 'dist/assets/compressed'}
				]

		aws: grunt.file.readJSON 'aws.json'	

		aws_s3:
			options:
				accessKeyId: '<%= aws.key %>'
				secretAccessKey: '<%= aws.secret %>'
				region: "eu-west-1"
				uploadConcurrency: 5

			production:
				options: 
					bucket: '<%= aws.production_bucket %>'
					params:
						CacheControl: "no-transform, public, max-age=604800, s-maxage=604800"
				files: [
					{src: 'dist/index.html', dest: 'index.html', params: {CacheControl: 'no-transform, public, max-age=300, s-maxage=300'} }
					{expand: true, cwd: 'dist/assets/compressed', src: ['*.css'], dest: '/assets', params: {ContentEncoding: 'gzip'}}
					{expand: true, cwd: 'dist/assets/compressed', src: ['*.js'], dest: '/assets', params: {ContentEncoding: 'gzip', ContentType: 'application/javascript; charset=utf-8'}}
					{expand: true, cwd: 'dist/img/', src: ['**'], dest: 'img/', stream: true}
					{expand: true, cwd: 'dist/assets/font/', src: ['**'], dest: 'assets/font'}
				]		
		
	
	grunt.registerTask 'deploy:production', ['compress', 'aws_s3:production', 'exec']		